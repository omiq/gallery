const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let TagSchema = new Schema(
  {
    tag: { type: String, required: true }
  },
  {
    collection: "tags"
  }
);

// Export the model
module.exports = mongoose.model("Tag", TagSchema);
