const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let ImageSchema = new Schema(
  {
    title: { type: String, required: true, max: 100 },
    location: { type: String, required: true, max: 150 },
    tags: { type: Array, required: true },
    file: { type: String, required: true }
  },
  {
    collection: "image"
  }
);

// Export the model
module.exports = mongoose.model("Image", ImageSchema);
