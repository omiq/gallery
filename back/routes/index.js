var express = require("express");
var router = express.Router();
var multer = require("multer");

var mongoose = require("mongoose");
var ImageModel = require(".././models/ImageModel");
var TagModel = require(".././models/TagModel");

// File Upload function
var storage = multer.diskStorage({
  destination: "uploads",
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + Date.now() + "." + file.mimetype.split("/")[1]
    );
  }
});
var upload = multer({ storage: storage });

/* GET home page. */
router.get("/", function(req, res, next) {
  res.render("index", { title: "Express!" });
});

/* Upload File */
router.post("/save", upload.single("image"), function(req, res, next) {
  let newImage = ImageModel({
    title: req.body.title,
    location: req.body.location,
    tags: req.body.tags,
    file: req.file.filename
  });

  newImage.save(function(err) {
    if (err) res.send(err);
    res.send("Image created!");
  });
});

/* Upload Tag */
router.post("/addtag", function(req, res, next) {
  let newTag = TagModel({
    tag: req.body.tag
  });

  newTag.save(function(err) {
    if (err) {
      console.log(err);
      res.send(err);
    }

    res.send("Tag created!");
  });
});

/* Get Tags */
router.get("/tag", function(req, res, next) {
  TagModel.find({}).then(function(users) {
    let tags = [];

    users.forEach(function(user) {
      tags.push(user["tag"]);
    });
    res.send(tags);
  });
});

/* Get Images */
router.get("/image", function(req, res, next) {
  ImageModel.find({}).then(function(users) {
    res.send(users);
  });
});

/* Get Images */
router.post("/delete", function(req, res, next) {
  console.log(req.body.fileName);
  ImageModel.findOneAndRemove({
    file: req.body.fileName
  })
    .then(response => {
      // console.log(response);
      res.send("Deleted");
    })
    .catch(err => {
      res.send(err);
      console.error(err);
    });
});

module.exports = router;
