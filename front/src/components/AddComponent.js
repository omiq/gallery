import React, { Component } from "react";
import ImageForm from "../ui/ImageForm.js";
import TagForm from "../ui/TagForm.js";

const axios = require("axios");

export default class HomeComponent extends Component {
  constructor(props) {
    super(props);
    this.updateNames = this.updateNames.bind(this);
  }
  state = { names: [] };

  componentDidMount() {
    axios.get("http://localhost:5000/tag").then((res, rej) =>
      this.setState({
        names: res.data
      })
    );
  }

  updateNames(name) {
    this.setState({
      names: [...this.state.names, name]
    });
  }

  render() {
    return (
      <>
        <h1 style={{ textAlign: "center" }}>Add Image</h1>
        <ImageForm names={this.state.names} />
        <h3 style={{ textAlign: "center" }}>Add Tag</h3>
        <TagForm updateNames={this.updateNames} />
      </>
    );
  }
}
