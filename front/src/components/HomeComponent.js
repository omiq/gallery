import React, { Component } from "react";
import Cards from "../homeui/Cards.js";

const axios = require("axios");

export default class HomeComponent extends Component {
  state = { images: [] };
  componentDidMount() {
    axios.get("http://localhost:5000/image").then((res, rej) => {
      this.setState({ images: res.data });
    });
  }
  handleDelete = () => {
    axios.get("http://localhost:5000/image").then((res, rej) => {
      this.setState({ images: res.data });
    });
  };

  render() {
    const style = {
      display: "flex"
    };
    let cards = this.state.images.map((item, i) => (
      <Cards key={i} image={item} handleDelete={this.handleDelete} />
    ));
    return (
      <div className="App">
        <h1 style={{ textAlign: "center" }}>All Images</h1>
        <div style={style}>{cards}</div>
      </div>
    );
  }
}
