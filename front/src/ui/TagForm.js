import React from "react";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";

const axios = require("axios");

export default class UploadForm extends React.Component {
  state = {
    tags: ""
  };

  handleTagChange(e) {
    this.setState({ tags: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    // console.log(this.state);
    let that = this;

    axios({
      method: "post",
      url: "http://localhost:5000/addtag",
      data: {
        tag: this.state.tags
      }
    })
      .then(function(response) {
        //handle success
        console.log(response);
        that.props.updateNames(that.state.tags);
        that.setState({
          tags: ""
        });
      })
      .catch(function(response) {
        //handle error
        console.log(response);
      });
  }
  render() {
    const form = {
      display: "flex",
      flexDirection: "column",
      width: "50%",
      margin: "auto"
    };

    return (
      <form id="imageForm" style={form} onSubmit={this.handleSubmit.bind(this)}>
        <Input
          type="text"
          name="title"
          placeholder="Give Title"
          value={this.state.tags}
          onChange={this.handleTagChange.bind(this)}
        />
        <Button type="submit">Add</Button>
      </form>
    );
  }
}
