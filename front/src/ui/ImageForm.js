import React from "react";
import UploadForm from "./UploadFrom.js";

const axios = require("axios");

export default class ImageForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: {},
      location: "",
      title: "",
      tags: []
    };
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleTagChange = this.handleTagChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }
  handleLocationChange(e) {
    this.setState({ location: e.target.value });
  }
  handleFileChange(e) {
    this.setState({ file: e.target.files[0] });
  }
  handleTagChange(e) {
    this.setState({ tags: e.target.value });
    // console.log(this.state.tags);
  }
  handleSubmit(e) {
    e.preventDefault();

    const bodyFormData = new FormData();
    bodyFormData.append("title", this.state.title);
    bodyFormData.append("location", this.state.location);
    bodyFormData.append("tags", this.state.tags);
    bodyFormData.append("image", this.state.file);
    // console.log(bodyFormData);

    let that = this;
    axios({
      method: "post",
      url: "http://localhost:5000/save",
      data: bodyFormData,
      config: { headers: { "Content-Type": "multipart/form-data" } }
    })
      .then(function(response) {
        //handle success
        console.log(response);
        that.setState({
          title: "",
          location: "",
          tags: []
        });
      })
      .catch(function(response) {
        //handle error
        console.log(response);
      });
  }

  render() {
    const props = {
      handleFileChange: this.handleFileChange,
      handleLocationChange: this.handleLocationChange,
      handleTagChange: this.handleTagChange,
      handleTitleChange: this.handleTitleChange,
      handleSubmit: this.handleSubmit,
      title: this.state.title,
      location: this.state.location,
      file: this.state.file,
      tags: this.state.tags,
      names: this.props.names
    };
    return <UploadForm {...props} />;
  }
}
