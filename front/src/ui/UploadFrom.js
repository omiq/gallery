import React from "react";
import Input from "@material-ui/core/Input";
import Button from "@material-ui/core/Button";
import Select from "../ui/Select.js";

const UploadForm = props => {
  const form = {
    display: "flex",
    flexDirection: "column",
    width: "50%",
    margin: "auto"
  };
  let {
    handleFileChange,
    handleLocationChange,
    handleTagChange,
    handleTitleChange,
    handleSubmit,
    title,
    location,
    file,
    tags,
    names
  } = props;
  return (
    <form id="imageForm" style={form} onSubmit={handleSubmit}>
      <input
        accept="image/*"
        style={{ display: "none" }}
        id="raised-button-file"
        name="image"
        type="file"
        onChange={handleFileChange}
      />
      <label htmlFor="raised-button-file">
        <Button variant="raised" component="span">
          Choose Image
        </Button>
      </label>
      <Input
        type="text"
        name="title"
        placeholder="Give Title"
        value={title}
        onChange={handleTitleChange}
      />
      <Input
        type="text"
        name="location"
        placeholder="Give Location"
        value={location}
        onChange={handleLocationChange}
      />
      <Select tags={tags} handleTagChange={handleTagChange} names={names} />
      <Button type="submit">Add</Button>
    </form>
  );
};

export default UploadForm;
