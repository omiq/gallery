import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

import HomeComponent from "./components/HomeComponent.js";
import AddComponent from "./components/AddComponent.js";

import NavBar from "./ui/NavBar.js";

const App = () => (
  <Router>
    <main>
      <NavBar />

      <Route exact path="/" component={HomeComponent} />
      <Route path="/add" component={AddComponent} />
      {/* <Route path="*" component={<div>No content</div>} /> */}
    </main>
  </Router>
);

export default App;
