import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import Modal from "@material-ui/core/Modal";

const axios = require("axios");

function rand() {
  return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
  const top = 50 + rand();
  const left = 50 + rand();

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`
  };
}

const styles = theme => ({
  card: {
    maxWidth: 345,
    minWidth: 250,
    margin: "2%"
  },
  media: {
    height: 140
  },
  custom: { width: "100%" },
  paper: {
    position: "absolute",
    width: theme.spacing.unit * 25,
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4
  }
});

class Cards extends React.Component {
  state = {
    open: false,
    show: false,
    file: this.props.image.file
  };

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false, show: false });
  };
  showButton = () => {
    this.setState({ show: true });
  };
  hideButton = () => {
    this.setState({ show: false });
  };
  deleteImage = () => {
    // console.log(this.state.file);
    let that = this;
    axios({
      method: "post",
      url: "http://localhost:5000/delete",
      data: {
        fileName: this.state.file
      }
    })
      .then(function(response) {
        //handle success
        console.log(response);
        that.props.handleDelete();
      })
      .catch(function(response) {
        //handle error
        console.log(response);
      });
  };

  render() {
    const { image, classes } = this.props;
    const fileName = "http://localhost:5000/uploads/" + image.file;
    let hover = { visibility: this.state.show ? "visible" : "hidden" };
    return (
      <Card
        className={classes.card}
        onMouseEnter={this.showButton}
        onMouseLeave={this.hideButton}
      >
        <CardActionArea className={classes.custom}>
          <CardMedia
            className={classes.media}
            image={fileName}
            title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="display1" component="h2">
              {image.title}
            </Typography>
          </CardContent>
        </CardActionArea>
        <CardActions style={hover}>
          <Button
            variant="outlined"
            size="small"
            color="primary"
            onClick={this.handleOpen}
          >
            Deatails
          </Button>
          <Button
            size="small"
            color="secondary"
            variant="outlined"
            onClick={this.deleteImage}
          >
            Delete
          </Button>
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={this.handleClose}
          >
            <div style={getModalStyle()} className={classes.paper}>
              <Typography gutterBottom variant="title" component="h2">
                {image.title}
              </Typography>
              <Typography variant="subheading">{image.location}</Typography>
              <Typography variant="caption">
                {image.tags.toString().replace(",", " ")}
              </Typography>
            </div>
          </Modal>
        </CardActions>
      </Card>
    );
  }
}

Cards.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Cards);
